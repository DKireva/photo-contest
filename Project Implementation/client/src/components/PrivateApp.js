import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import AdminDashboard from "./private/Admin/AdminDashboard";
import React from 'react';
import { useAuth } from "../auth/authContext";
import JunkieDashboard from "./private/CommonUser/JunkieDashboard";
import HomePage from "./public/Home/HomePage";
import UserPage from './private/CommonUser/UserPage';
import OwnUserPage from './private/CommonUser/OwnUserPage';
import Header from '../components/Header/Header';
import Footer from "./Footer/Footer";
import ContestPage from "../components/Contest/ContestPage";


const PrivateApp = () => {

    const { user } = useAuth();

    return (
        <Router>
            <Header />
            <Switch>
                {user.role === 'Organizer' && <Route path="/login"><AdminDashboard /></Route>}
                {user.role === 'Junky' && <Route path="/login"><JunkieDashboard /></Route>}
                {user.role === 'Organizer' && <Route path="/dashboard"><AdminDashboard /></Route>}
                {user.role === 'Junky' && <Route path="/dashboard"><JunkieDashboard /></Route>}
                <Route path="/home">
                    <HomePage />
                </Route>
                <Route exact path="/users/:id">
                    <UserPage />
                </Route>
                <Route exact path="/users/:id/me">
                    <OwnUserPage />
                </Route>
                <Route path="/contests/:id">
                    <ContestPage />
                </Route>
                {/* {user.role === 'Junky' && <Route path="/contests/:id"><ContestPage /></Route>} */}
            </Switch>
            <Footer />
        </Router>
    )

}


export default PrivateApp;