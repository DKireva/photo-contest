import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

const PhotoDetails = props => {
    const { id, title, category, comment, is_fit, score, image, contest_id, user_id, remove } = props;

    return (
        <div id={id} className='ContestDetails'>
            <div>Category: {category} </div>
            <div>Contest: {contest_id}</div>
       Photo Title:<NavLink to={`/contests/${id}`}>{title}</NavLink>
            <div>Comment: {comment}</div>
            <div>Sutable for the Contest: {is_fit}</div>
            <div>Score: {score}</div>
            <div>User: {user_id}</div>
            <div>Photo: {image}</div>
            {/* <button onClick={remove}>Delete</button> */}
            {props.children}
        </div>
    );
}

PhotoDetails.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    cetegory: PropTypes.string,
    comment: PropTypes.string,
    is_fit: PropTypes.any,
    score: PropTypes.string,
    user_id: PropTypes.number,
    contest_id: PropTypes.number,
    image: PropTypes.string,
    deleteContest: PropTypes.func
};

export default PhotoDetails;