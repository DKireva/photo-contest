import React, { useState } from 'react';
import { Button } from "@material-ui/core";
import { NavLink } from 'react-router-dom';

const UploadPhoto = (props) => {
    const [file, setFile] = useState(null);

    const upload = () => {
        const token = localStorage.getItem('token');

        if (file) {
            const formData = new FormData();
            formData.set('image', file);

            fetch(`http://localhost:3001/contests/${props.contestId}/photo/${props.photoId}`, {
                method: 'POST',
                headers: { Authorization: `Bearer ${token}` },
                body: formData
            })
                .then(result => result.json())
                .then((data) => {
                    alert('You participated successfully in the Contest! You are not allowed to upload more photos for the Contest!')
                    console.log(data);
                })
        }
    };

    return (
        <form>
            <input type="file" onChange={e => setFile(e.target.files[0])} />
            <br />
            <br />
            <Button onClick={upload} variant="contained" color="primary" component="span" >
                {/* <NavLink to={`/contests/${props.contestId}/photo/${props.photoId}`}>Save Picture</NavLink> */}
                <NavLink to="/dashboard">Save Picture</NavLink>
            </Button>
        </form>
    )
}
export default UploadPhoto;
