import React, { useState, useEffect } from 'react';
import useAuthorizedFetch from '../../custom-hooks/useAuthorizedFetch';
import ParticipateInContest from './ParticipateInContest';
import { useParams } from 'react-router-dom';
import { BASE_URL } from '../../common/constants';
import Loader from '../Loader/Loader';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import './Contest.css'
import TextField from '@material-ui/core/TextField';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';


const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    },
}));

const ContestPage = () => {
    const classes = useStyles();
    const { id } = useParams();
    const [value, setValue] = useState(2);
    
    useEffect(() => {
        fetch(`http://localhost:3001/contest/${id}/rating`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    rating: value
                  })
            })
            .then(r => r.json())
            .then((data) => {
              console.log(data.rating);
               
            })
        
    }, [value])
    
    const contest = useAuthorizedFetch(`${BASE_URL}/contests/${id}`);

    if (contest.isLoading) {
        return (
            <>
                <div className={classes.root}>
                    <Loader />
                </div>
            </>
        )
    }
    
 
    return (
        <div className={classes.root} id="body">
            <Paper className={classes.paper} id="contest">
                <Grid container spacing={2}>
                    <Grid item>
                        <ButtonBase className={classes.image} id="contest-image">
                            <img className={classes.img} alt="complex" src={`http://localhost:3001/public/${contest.data.image}`} />
                            
                        </ButtonBase>
                        <Box component="fieldset" mb={3} borderColor="transparent">
                            <Typography component="legend">Rating</Typography>
                            <Rating
                            name="simple-controlled"
                            value={contest.data.rating}
                            onChange={(click, newValue) => {
                                setValue(newValue)
                                }}
                            
                           
                            />
                    </Box>
                    </Grid>
                    <Grid item xs={12} sm container>
                        <Grid item xs container direction="column" spacing={2}>
                            <Grid item xs>
                            <TextField
                                id="outlined-helperText"
                                label="Category:"
                                margin="normal"
                                size="small"
                                defaultValue= {contest.data.category}
                                variant="outlined"
                             />
                            <TextField
                                id="outlined-helperText"
                                label="Title:"
                                margin="normal"
                                size="small"
                                defaultValue= {contest.data.title}
                                variant="outlined"
                            />
                            <TextField
                                id="outlined-helperText"
                                label="Type:"
                                margin="normal"
                                size="small"
                                defaultValue= {contest.data.is_open}
                                variant="outlined"
                             />
                            </Grid>
                            <Grid item>
                                <Typography variant="body2" style={{ cursor: 'pointer' }}>
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid item>
                            <TextField
                                    id="outlined-helperText"
                                    label="Description:"
                                    margin="normal"
                                    size="small"
                                    defaultValue= {contest.data.description}
                                    variant="outlined"
                             />
                            <TextField
                                    id="outlined-helperText"
                                    label="Phase One Deadline:"
                                    margin="normal"
                                    size="small"
                                    defaultValue= {contest.data.date_1}
                                    variant="outlined"
                             />
                           
                        </Grid>
                    </Grid>
                   

                    {/* {user.data.roleid === 1 */}
                    {/* <Button variant="contained" color="default" style={{ height: "31px", margin: "10px 10px" }}>
                            <Link className='nav-home' to='/home'>Participate in the Contest</Link>
                        </Button> */}
                    {/* ? <div><img className="avatar" src={`http://localhost:3001/public/${user.data.avatar}`}  alt="avatar" /> <h5>Rank: <CheckRank score={user.data.score} roleid={user.data.roleid}/></h5></div> */}
                    {/* <div><Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}></Button> */}
                    {/* <Button variant="contained" color="default" style={{ height: "31px", margin: "10px 10px" }} onClick={handleClick}></Button> */}

                    {/* <FileUpload contestId={contest.data.id} /><CheckBoxes /> Enroll */}
                    {/* </div> */}
                    {/* } */}
                    {/* <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}></Button> */}
                    {/* <Button variant="contained" color="default" style={{ height: "31px", margin: "10px 10px" }} onClick={handleClick}> */}

                    {/* <FileUpload contestId={contest.data.id} /><CheckBoxes /> Enroll */}
                    {/* Upload your photo */}
                    {/* </Button> */}

                </Grid>
                <div className="create-btn">
                    <ParticipateInContest />
                </div>
            </Paper>
        </div>
    );
}

export default ContestPage;