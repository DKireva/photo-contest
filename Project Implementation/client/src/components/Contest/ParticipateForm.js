import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import { makeStyles } from '@material-ui/core/styles';
import { useParams } from 'react-router-dom';
import { BASE_URL } from '../../common/constants';
import AppError from '../AppError/AppError';
import UploadPhoto from './UploadPhoto';
import { useAuth } from '../../auth/authContext';

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 250,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));
const ParticipateForm = () => {

    const { id } = useParams();
    const { user } = useAuth();

    const photo = {
        title: '',
        comment: '',
        is_fit: '0',
        score: '0',
        contest_id: '',
        user_id: ''
    };

    const classes = useStyles();
    const [error, setError] = React.useState(null);
    const [photoId, setPhotoId] = React.useState(0);

    const createParticipation = () => {
        const token = localStorage.getItem('token');

        if (!photo.title) {
            alert(`Enter a valid photo title!`);
            return;
        }

        if (!photo.comment) {
            alert(`Enter a comment!`);
            return;
        }

        fetch(`${BASE_URL}/contests/${id}/photo`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(photo),
        })
            .then(response => response.json())
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                }
                setPhotoId(result.photo.id);
                alert('Plese, upload your photo!')
            })
            .catch(error => setError(error.message));
    };

    const updatePhotoProp = (prop, value) => {
        photo[prop] = value;
    };

    if (error) {
        return (
            <AppError message={error} />
        );
    }

    return (
        <React.Fragment>
            <div className="modal-view">
                <Typography variant="h6" gutterBottom>
                    Fill in the participation form
                    {updatePhotoProp('contest_id', id)}
                    {updatePhotoProp('user_id', user.sub)}
                </Typography>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                        <TextField
                            required
                            id="title"
                            name="title"
                            label="Title"
                            fullWidth
                            autoComplete="Title"
                            onChange={(e) => updatePhotoProp('title', e.target.value)}
                        />
                    </Grid>
                    <Grid container spacing={3}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                required
                                id="comment"
                                name="comment"
                                label="Comment"
                                fullWidth
                                autoComplete="Comment"
                                onChange={(e) => updatePhotoProp('comment', e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <UploadPhoto contestId={id} photoId={photoId}/>
                        </Grid>
                        <Grid>
                            <Button
                                variant="contained"
                                color="primary"
                                size="small"
                                className={classes.button}
                                startIcon={<SaveIcon />}
                                onClick={createParticipation}
                            >
                                Save
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        </React.Fragment>
    );
}

export default ParticipateForm;