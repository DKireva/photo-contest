import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';


const ContestDetails = props => {
  const { id, title, category, description, is_open, date_1, date_2, date_3, image, remove } = props;

  return (
    <div id={id} className='ContestDetails'>
      <div>Contest picture: {image}</div>
      Title:<NavLink to={`/contests/${id}`}>{title.toUpperCase()}</NavLink> <br/>
                     <TextField
                                id="outlined-helperText"
                                label="Category:"
                                margin="normal"
                                fullWidth="true"
                                size="small"
                                defaultValue= {category}
                                variant="outlined"
                             />
                      <TextField
                                id="outlined-helperText"
                                label="Description:"
                                margin="normal"
                                fullWidth="true"
                                size="small"
                                defaultValue= {description}
                                variant="outlined"
                             />
                            
                       <TextField
                                id="outlined-helperText"
                                label="Type:"
                                margin="normal"
                                fullWidth="true"
                                size="small"
                                defaultValue= {is_open}
                                variant="outlined"
                              />
                        <TextField
                                id="outlined-helperText"
                                label="Closing date for Phase 1:"
                                margin="normal"
                                size="small"
                                defaultValue= {date_1}
                                variant="outlined"
                               />
                        <TextField
                                id="outlined-helperText"
                                label="Closing date for Phase 2:"
                                margin="normal"
                                size="small"
                                defaultValue= {date_2}
                                variant="outlined"
                               />
                         <TextField
                                id="outlined-helperText"
                                label="Closing date for Phase 3:"
                                margin="normal"
                                size="small"
                                defaultValue= {date_3}
                                variant="outlined"
                             />
  
                       {props.children}
    </div>
  );
}

ContestDetails.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
  cetegory: PropTypes.string,
  description: PropTypes.string,
  is_open: PropTypes.any,
  date_1: PropTypes.string,
  date_2: PropTypes.string,
  date_3: PropTypes.string,
  picture: PropTypes.string,
  deleteContest: PropTypes.func,
  updateContest: PropTypes.func
};

export default ContestDetails;