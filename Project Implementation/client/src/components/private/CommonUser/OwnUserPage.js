import React from 'react';
import { useParams } from 'react-router-dom';
import useAuthorizedFetch from '../../../custom-hooks/useAuthorizedFetch';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import CheckRank from '../../../common/CheckRank/CheckRank';
import './UserPage.css';
import FileUpload from './FileUpload';
import TextField from '@material-ui/core/TextField';
import UserContests from '../../Contest/UserContests';


const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const OwnUserPage = () => {
  const classes = useStyles();
  const { id } = useParams();
  const user = useAuthorizedFetch(`http://localhost:3001/users/${id}/me`);
  

  if (user.isLoading) {
    return (
      <>
         <div className={classes.root}>
             <CircularProgress />
         </div>
      </>
    )
  }
  return (
    <React.Fragment>
      <CssBaseline />
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <Container maxWidth="sm" >
          <div className="single-avatar">
                {user.data.avatar
                    ? <div><img className="avatar" src={`http://localhost:3001/public/${user.data.avatar}`} alt="avatar" /> <h5>Rank: <CheckRank score={user.data.score} roleid={user.data.roleid}/></h5></div>
                    : null
              }
              
            <div className="text-container">
            <TextField
          id="outlined-helperText"
          label="USERNAME"
          margin = "normal"
          defaultValue= {user.data.username.toUpperCase()}
          variant="outlined"
                />

                <TextField
          id="outlined-helperText"
          label="FIRST NAME"
          margin = "normal"
          defaultValue= {user.data.first_name}
          variant="outlined"
                />
                <TextField
          id="outlined-helperText"
          label="SECOND NAME"
          margin = "normal"
          defaultValue= {user.data.second_name}
          variant="outlined"
                />
                <TextField
          id="outlined-helperText"
          label="EMAIL"
          margin = "normal"
          defaultValue= {user.data.email}
          variant="outlined"
        />
          <FileUpload />
            </div>
            </div>
          </Container>
        </div>
        <Container className={classes.cardGrid} maxWidth="md">
          {/* End hero unit */}
          {/* <Grid container spacing={4}>
            {cards.map((card) => (
              <Grid item key={card} xs={12} sm={6} md={4}>
                <Card className={classes.card}>
                  <CardMedia
                    className={classes.cardMedia}
                    image="https://source.unsplash.com/random"
                    title="Image title"
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      Heading
                    </Typography>
                    <Typography>
                      This is a media card. You can use this section to describe the content.
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button size="small" color="primary">
                      View
                    </Button>
                    <Button size="small" color="primary">
                      Edit
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid> */}
          <UserContests />
        </Container>
      </main>
    </React.Fragment>
  );
}

export default OwnUserPage;