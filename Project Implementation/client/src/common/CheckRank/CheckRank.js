import React from 'react';

const CheckRank = (props) => {
 
 
  return (
  <>
          {(() => {
                    if (props.roleid === 3) {
                      return (
                        <p>Admin</p>
                      )
                    } else if (props.score <= 50) {
                      return (
                        <p>Junkie: ({ props.score}pts) </p>
                      )
                    } else if (props.score > 50 && props.score <= 150) {
                      return (
                        <p>Enthusiast: ({ props.score}pts) </p>
                      )
                    } else if (props.score > 150 && props.score <= 1000) {
                      return (
                        <p>Master: ({ props.score}pts) </p>
                      )
                    } else {
                      return (
                        <p>Wise and Benevolent Photo Dictator: ({ props.score}pts) </p>
                      )
                    }
                  })()}
    
   
      </>
)
}

export default CheckRank;