import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { PORT } from './config.js';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import usersController from './controllers/users-controller.js';
import authController from './controllers/auth-controller.js';
import contestController from './controllers/contest-controller.js';


const app = express();

passport.use(jwtStrategy);

app.use(cors(), bodyParser.json());
app.use(passport.initialize());
app.use('/users', usersController);
app.use('/auth', authController);
app.use('/public', express.static('images'));

app.use('/', contestController);

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' }),
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));