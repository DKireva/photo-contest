
export const DB_CONFIG = {
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '12345',
    database: 'photo-contest'
};

export const PORT = 3001;


export const PRIVATE_KEY = 'top_secret_key';

// 60 mins * 60 secs
export const TOKEN_LIFETIME = 60 * 60; 

export const DEFAULT_USER_ROLE = 'Junky';
