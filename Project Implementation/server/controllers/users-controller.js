import express from 'express';
import usersService from '../services/users-service.js';
import usersData from '../data/users-data.js';
import serviceErrors from '../services/service-errors.js';
import { createValidator, createUserSchema, updateUserSchema } from '../validations/index.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import multer from 'multer';
import storage from './../storage.js';



const usersController = express.Router();

usersController
    // get all users with searching
    .get('/', async (req, res) => {
        const { search } = req.query;
        const users = await usersService.getAllUsers(usersData)(search);

        res.status(200).send(users);
    })
    // get user by id
    .get('/:id',
        authMiddleware,
        roleMiddleware('Organizer'),
        async (req, res) => {
            console.log(res)
            const { id } = req.params;

            const { error, user } = await usersService.getUserById(usersData)(+id);

            if (error === serviceErrors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            } else {
                res.status(200).send(user);
            }
        })

     // get own user by id
     .get('/:id/me',
     authMiddleware,
     roleMiddleware('Junky'),
     async (req, res) => {
         console.log(res)
         const { id } = req.params;

         const { error, user } = await usersService.getUserById(usersData)(+id);

         if (error === serviceErrors.RECORD_NOT_FOUND) {
             res.status(404).send({ message: 'User not found!' });
         } else {
             res.status(200).send(user);
         }
     })
    // create user
    .post('/',
        createValidator(createUserSchema),
        async (req, res) => {
            const createData = req.body;

            const { error, user } = await usersService.createUser(usersData)(createData);
            if (error === serviceErrors.DUPLICATE_RECORD) {
                res.status(409).send({ message: 'Name not available' });
            } else {
                res.status(201).send(user);
            }
        })
    
    

    // update user by id
    .put('/:id',
        createValidator(updateUserSchema),
        async (req, res) => {
            const { id } = req.params;
            const updateData = req.body;

            console.log(updateData);

            const { error, user } = await usersService.updateUser(usersData)(+id, updateData);

            if (error === serviceErrors.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            } else if (error === serviceErrors.DUPLICATE_RECORD) {
                res.status(409).send({ message: 'Name not available' });
            } else {
                res.status(200).send(user);
            }
        })
    // delete user by id
    .delete('/:id', async (req, res) => {
        const { id } = req.params;
        const { error, user } = await usersService.deleteUser(usersData)(+id);

        if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'User not found!' });
        } else {
            res.status(200).send(user);
        }
    })


    // upload avatar
    .post('/:id/avatar',
        authMiddleware,
        multer({ storage: storage }).single('avatar'),
        async (req, res,) => {
            const { error } = await usersService.updateAvatar(usersData)(
                req.params.id,
                req.file.filename
            );

            if (error) {
                res.sendStatus(500);
            } else {
                res.status(200).send({
                    path: req.file.filename
                });
            }
        });

export default usersController;
